<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Meu título - @yield('titulo')</title>

      
    </head>
    <body>
        @section('barralateral')
            Esta parte da seção é do template PAI
        @show    
        <div>
            @yield('conteudo')
        </div>
    </body>
</html>
