@extends('layouts.meulayout')

@section('minha_secao_produtos')
    <a href="/opcoes/1"  class="btn btn-primary btn-sn"  role="button" aria-disabled="true">Azul</a>
    <a href="/opcoes/2"  class="btn btn-danger  btn-sn"  role="button" aria-disabled="true">Vermelho</a>
    <a href="/opcoes/3"  class="btn btn-success btn-sn"  role="button" aria-disabled="true">Verde</a>
    <a href="/opcoes/4"  class="btn btn-warning btn-sn"  role="button" aria-disabled="true">Amarelo</a>
    <a href="/opcoes/10" class="btn btn-light   btn-sn"  role="button" aria-disabled="true">Branco</a>   
@endsection