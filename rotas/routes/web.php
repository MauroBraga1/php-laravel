<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "<h1>Laravel</h1>";
});

Route::get('/ola', function () {
    return "<h1>Seja bem vindo</h1>";
});


Route::get('/ola/sejabemvindo', function () {
    return view('welcome');
});

Route::get('/nome/{nome}/{sobrenome}', function($nome,$sobrenome){
    return "<h1> Olá ,$nome $sobrenome </h1>";
});

Route::get('/repetir/{nome}/{n}', function($nome,$n){
    if(is_integer($n)){
        for($i=0; $i <$n;$i++){
            echo "<h1> Olá ,$nome  </h1>"; 
        }
    }else{
        echo 'Não é um número';
    }
});

Route::get('/seunomecomregra/{nome}/{n}', function($nome,$n){
   for($i=0; $i <$n;$i++){
            echo "<h1> Olá ,$nome ($i) </h1>"; 
    }
    
})->where('n', '[0-9]*')->where('nome', '[A-Za-z]*');

Route::get('/seunomesemregra/{nome?}/', function($nome=null){
    if(isset($nome)){
        echo "<h1> Olá ,$nome </h1>"; 

    }else{
        echo 'Voce nao passou nenhum nome';
    }
         
 })->where('nome', '[A-Za-z]*');

Route::prefix('app')->group( function(){
    Route::get("/",function(){
        return "Página principal do App";
    });
    Route::get("profile", function(){
        return "Página profile";
    });
    Route::get("about", function(){
        return "Meu about";
    });
});

Route::redirect('/aqui','/ola',301);

Route::view('/hello',  'hello');

Route::view('/viewhello',  'hellonome',['nome' => 'Mauro', 'sobrenome'=>'Braga']);

Route::get('/hellonome/{nome}/{sobrenome}', function($nome, $sobrenome){
    return view('hellonome',['nome' => $nome, 'sobrenome'=>$sobrenome]);
});

Route::get('/rest/hello', function(){
    return "Hello (GET)";
});

Route::DELETE('/rest/hello', function(){
    return "Hello (DELETE)";
});

Route::put('/rest/hello', function(){
    return "Hello (PUT)";
});

Route::patch('/rest/hello', function(){
    return "Hello (Pach)";
});

Route::options('/rest/hello', function(){
    return "Hello (Options)";
});

use Illuminate\Http\Request;


Route::post('/rest/imprimir', function(Request $request){
    $nome = $request->input('nome');
    $idade = $request->input('idade');

    return "Hello $nome ($idade)::(POST)"; 
});

Route::match(['get','post'],'/rest/hello2', function(){
    return "Hello World 2";
});

Route::any('/rest/hello3', function(){
    return "Hello World 3";
});

Route::get('/produtos', function () {
    echo "<h1>Produtos</h1>";
    echo "<ol>";
    echo "<li>Notebook </li>";
    echo "<li>Impressora </li>";
    echo "<li>Mouse </li>";
    echo "</ol>";
})->name('meusprodutos');


Route::get('/linkprodutos', function () {
    $url = route('meusprodutos');
    echo "<a href=\"" . $url . "\">Meus Produtos</a>"; 
});

Route::get('/redirecionarprodutos', function () {
    return redirect()->route('meusprodutos');
});