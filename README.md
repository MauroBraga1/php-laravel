# p
#criar 

#INSTALAR PHP REPOSITORY
sudo add-apt-repository ppa:ondrej/php

#Criando Projeto 
composer create-project --prefer-dist laravel/laravel blog "5.4.*"

#Rodar projeto
php artisan serve

#publicar vendor
php artisan vendor:publish

#Laravel 5 Repositories
#Installar
#https://github.com/andersao/l5-repository
# Ao final criar a pasta Entities no diretorio app
composer require prettus/l5-repository

#comando de criação
php artisan make

#comando de criação entity
php artisan make:entity Users
php artisan make:model UserSocial -m

#comando de criação de estrutura do banco de dados
php artisan migrate


#adicionando o componentes de form
composer require laravelcollective/html "5.4.*"

#Controlador
php artisan make:controller ClienteControlador --resource


#ADICIONANDO BOOTSTRAP
yarn install
#cria os arquivos
npm run dev 

#arquivo que possui configuração de banco de dados
.env

#migrations
php artisan make:migration create_products
php artisan migrate
php artisan make:migration add_brand_to_products --table=products


